def simular_semiparabolico(vi): 
    posicion=[]
    G=9.8
    t=0
   
    while t<=2:
        x=round(vi*t,3)
        y=round(G*t**2/2, 3)
        t=t+0.2
        posicion.append([x, y])
    
    return posicion
r=simular_semiparabolico(34)

for posiciones in r:
    print ("x:", posiciones[0], "y:", posiciones[1])